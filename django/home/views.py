from handlers import cwb
from django.shortcuts import render
from django.utils.translation import gettext as _


def home(request):
    return render(
        request,
        "home/home.html",
        {"page_name": _("Inicio"), "corpora": cwb.corpora()},
    )


def about(request):
    return render(request, "home/about.html", {"page_name": _("Créditos")})


def error_400(request, exception):
    del exception
    return error_page(request, error_code=400)


def error_403(request, exception):
    del exception
    return error_page(request, error_code=403)


def error_404(request, exception):
    del exception
    return error_page(request, error_code=404)


def error_500(request):
    return error_page(request, error_code=500)


def error_page(request, error_code):
    msg = {
        400: _("Petición con sintaxis errónea."),
        403: _("Petición rechazada."),
        404: _("Recurso no encontrado."),
        500: _("Error interno del servidor."),
    }[error_code]
    context = {
        "page_name": _("Error %s") % error_code,
        "corpora": cwb.corpora(),
        "results": {"meta": {"search": {"msg": msg}}},
        "error_code": error_code,
    }
    return render(request, "results.html", context)
