from tests.common import assert_templates, assert_context
from django.test import Client


def test_home_view():
    c = Client()
    response = c.post("")
    assert response.status_code == 200  # noqa: PLR2004
    assert_templates(
        response.templates,
        "common/main common/user-button search/search-bar home/home",
    )
    assert_context(response.context, {"corpora": dict})


def test_about():
    c = Client()
    response = c.post("/about/")
    assert response.status_code == 200  # noqa: PLR2004
    assert_templates(
        response.templates,
        "common/main common/user-button home/about",
    )
    assert_context(response.context, {"page_name": str})
