from django.urls import path
from django.conf.urls import include

from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("about/", views.about, name="about"),
    path("accounts/", include("django.contrib.auth.urls")),
]
