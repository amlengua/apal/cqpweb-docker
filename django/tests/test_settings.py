from api.utils import flatten
from django.conf import settings


def test_settings():
    keys = set(flatten(settings.DCWB).keys())
    assert set() == keys.difference(
        {
            "SITE.NAME",
            "SITE.DESCRIPTION",
            "SITE.KEYWORDS",
            "SITE.OWNER",
            "SITE.URL",
            "USERS.STAFF.CONTEXT.LEFT",
            "USERS.STAFF.CONTEXT.RIGHT",
            "USERS.AUTH.CONTEXT.LEFT",
            "USERS.AUTH.CONTEXT.RIGHT",
            "USERS.ANON.CONTEXT.BOTH",
            "CQP.TEXT_ID",
            "CQP.S_TAG",
            "CQP.PAGINATOR",
        }
    )
