import re
import ast
import json
import inspect
from pathlib import Path


def assert_keys(obj1, *args):
    set1 = set(obj1.keys()) if hasattr(obj1, "keys") else set(obj1)
    if len(args) == 1:
        set2 = set(re.split(r"\s+", args[0].strip()))
    else:
        set2 = set(args)
    assert set1.difference(set2) == set()


def assert_values(obj, *args):
    vals = list(obj.values()) if hasattr(obj, "values") else list(obj)
    for i, arg in enumerate(args):
        if isinstance(arg, str):
            assert str(arg) in str(vals[i])
        elif isinstance(arg, type):
            assert arg is type(vals[i])
        else:
            assert arg == vals[i]


def assert_templates(obj, names):
    templates = {template.name.replace(".html", ""): None for template in obj}
    assert_keys(templates, names)


def assert_context(obj, dictionary):
    for key, val in dictionary.items():
        assert_values({key: obj[key]}, val)


def get_asset(suffix, parse=True):
    code = inspect.currentframe().f_back.f_code
    dirname = Path(code.co_filename).stem.split("_")[-1]
    methodname = code.co_name
    filename = "%s.%s.txt" % (methodname, suffix)
    path = Path(__file__).parent / "assets" / dirname / filename
    txt = path.read_text().strip()
    if parse:
        try:
            return ast.literal_eval(txt)
        except Exception:
            return json.loads(txt)
    else:
        return txt
