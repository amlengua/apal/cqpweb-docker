from django.contrib import admin
from django.contrib import messages
from django.contrib.admin import helpers
from django.contrib.admin.utils import model_ngettext
from django.core.exceptions import PermissionDenied
from django.template.response import TemplateResponse
from django.utils.translation import gettext as _
from django.utils.safestring import mark_safe
from docs.models import Doc, Corpus
from docs.utils import get_url
from docs.validators import (
    validate_json_metadata,
    validate_doc,
    validate_metadata,
)


@admin.action(
    permissions=["change"],
    description=_("Validar metadatos"),
)
def validate_metadatas(modeladmin, request, queryset):
    for obj in queryset:
        valid = False
        for corpus_id, data in obj.metadata.items():
            corpus = Corpus.objects.get(id=corpus_id)
            if corpus:
                valid = validate_json_metadata(corpus.metadata_schema, data)
                valid = validate_metadata(data, corpus_id)
            if not valid:
                break
        queryset.filter(id=obj.id).update(valid_metadata=valid)


@admin.action(
    permissions=["change"],
    description=_("Validar archivos"),
)
def validate_files(modeladmin, request, queryset):
    for obj in queryset:
        valid = False
        if obj.file:
            try:
                validate_doc(obj)
                valid = True
            except Exception:
                pass
        queryset.filter(id=obj.id).update(valid_file=valid)


@admin.action(
    permissions=["delete"],
    description=_("Eliminar items seleccionados"),
)
def delete_bulk(modeladmin, request, queryset):
    """Deletes all selected instances.

    NOTE: It is based on delete_selected from django/contrib/admin/actions.py
    check comments below starting with "MOD:" to see the diffs
    """
    opts = modeladmin.model._meta

    # Populate deletable_objects, a data structure of all related objects that
    # will also be deleted.
    (
        deletable_objects,
        model_count,
        perms_needed,
        protected,
    ) = modeladmin.get_deleted_objects(queryset, request)

    # The user has already confirmed the deletion.
    # Do the deletion and return None to display the change list view again.
    if request.POST.get("post") and not protected:
        if perms_needed:
            raise PermissionDenied
        n = queryset.count()
        if n:
            for obj in queryset:
                obj_display = str(obj)
                modeladmin.log_deletion(request, obj, obj_display)
            modeladmin.delete_queryset(request, queryset)
            modeladmin.message_user(
                request,
                _("Se eliminaron con éxito %(count)d %(items)s.")
                % {"count": n, "items": model_ngettext(modeladmin.opts, n)},
                messages.SUCCESS,
            )
        # Return None to display the change list page again.
        return None

    objects_name = model_ngettext(queryset)

    # MOD: start
    if opts.object_name == "Corpus":
        for corpus in queryset:
            for doc in Doc.objects.filter(corpora__id=corpus.id):
                msg = mark_safe(_("Documento: %s") % (get_url(doc)))
                if msg not in protected:
                    protected.append(msg)
    protected = sorted(protected)
    # MOD: end

    if perms_needed or protected:
        title = _("No se puede eliminar %(name)s") % {"name": objects_name}
    else:
        title = _("¿Estás seguro?")

    context = {
        **modeladmin.admin_site.each_context(request),
        "title": title,
        "subtitle": None,
        "objects_name": str(objects_name),
        "deletable_objects": [deletable_objects],
        "model_count": dict(model_count).items(),
        "queryset": queryset,
        "perms_lacking": perms_needed,
        "protected": protected,
        "opts": opts,
        "action_checkbox_name": helpers.ACTION_CHECKBOX_NAME,
        "media": modeladmin.media,
    }

    request.current_app = modeladmin.admin_site.name

    # Display the confirmation page
    return TemplateResponse(
        request,
        "docs/delete_bulk_confirmation.html",
        context,
    )
