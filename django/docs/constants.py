from django.utils.translation import gettext_lazy as _
from docs.utils import get_mobj

# NOTE: JSON Schema for metadata schemata
# Cfr. https://json-schema.org/overview/what-is-jsonschema
# Cfr. https://django-jsonform.readthedocs.io/en/stable/schema.html
METADATA_SCHEMA = {
    "type": "object",
    "properties": {
        "items": {
            "title": _("Metadatos"),
            "description": _(
                "Para agregar un tipo de metadato, da clic en «Add item»."
            ),
            "$ref": "#/$defs/mobjs",
        },
    },
    "$defs": {
        "mobjs": {
            "type": "list",
            "items": {
                "anyOf": [
                    get_mobj("string", _("Texto")),
                    get_mobj(
                        "number",
                        _("Número"),
                        default__type="number",
                        numtype={
                            "title": _("Tipo"),
                            "type": "string",
                            "helpText": _("Tipo de número."),
                            "choices": [
                                _("Cualquiera"),
                                _("Entero"),
                                _("Decimal"),
                            ],
                            "default": _("Cualquiera"),
                        },
                        minimum={
                            "title": _("Valor mínimo"),
                            "type": "number",
                            "required": False,
                            "default": 1,
                            "helpText": _(
                                "Para deshabilitar esta opción, usa '-1'."
                            ),
                        },
                        maximum={
                            "title": _("Valor máximo"),
                            "type": "number",
                            "required": False,
                            "default": -1,
                            "helpText": _(
                                "Para deshabilitar esta opción, usa '-1'."
                            ),
                        },
                    ),
                    get_mobj(
                        "string_id",
                        _("Identificador"),
                        title__helpText=_(
                            "Nombre necesario del metadato sin espacios."
                        ),
                        minimum={
                            "title": _("Extensión mínima de caracteres"),
                            "type": "integer",
                            "default": 18,
                            "helpText": _(
                                "Para deshabilitar esta opción, usa '-1'."
                            ),
                        },
                        maximum={
                            "title": _("Extensión máxima de caracteres"),
                            "type": "integer",
                            "default": -1,
                            "helpText": _(
                                "Para deshabilitar esta opción, usa '-1'."
                            ),
                        },
                    ),
                    get_mobj(
                        "string_datetime",
                        _("Fecha y hora"),
                        default__format="datetime",
                    ),
                    get_mobj(
                        "string_date", _("Fecha"), default__format="date"
                    ),
                    get_mobj("string_time", _("Hora"), default__format="time"),
                    get_mobj(
                        "string_choices",
                        _("Menú de opciones"),
                        choices={
                            "title": _("Opciones"),
                            "type": "list",
                            "items": {"type": "string"},
                            "minItems": 1,
                            "uniqueItems": True,
                            "required": True,
                            "helpText": "Opciones necesarias del metadato.",
                        },
                    ),
                    get_mobj(
                        "boolean",
                        _("Booleano"),
                        default__choices=[_("Verdadero"), _("Falso")],
                        default__default=_("Falso"),
                    ),
                    get_mobj(
                        "list",
                        _("Lista"),
                        minItems={
                            "title": _("Cantidad mínima de ítems"),
                            "type": "integer",
                            "default": 1,
                            "helpText": _(
                                "Para deshabilitar esta opción, usa '-1'."
                            ),
                        },
                        maxItems={
                            "title": _("Cantidad máxima de ítems"),
                            "type": "integer",
                            "default": -1,
                            "helpText": _(
                                "Para deshabilitar esta opción, usa '-1'."
                            ),
                        },
                        items={"title": _("Ítems"), "$ref": "#/$defs/mobjs"},
                    ),
                    get_mobj(
                        "object",
                        _("Lista mixta"),
                        properties={
                            "title": _("Ítems"),
                            "$ref": "#/$defs/mobjs",
                        },
                    ),
                ]
            },
            "minItems": 1,
            "uniqueItems": True,
        }
    },
}

# NOTE: Keys are classes or methods used in utils.load_schema
SUPPORTED_SCHEMATA = {
    "XMLSchema": _("XSD"),
    "DTD": _("DTD"),
    "check_schema": _("Esquema JSON"),
    "RelaxNG": _("Relax NG"),
    "Schematron": _("Schematron"),
}

# NOTE: Values are pipelines for data processing
SUPPORTED_FILES = {
    # Plain files
    "xml": ["get_doc"],
    "txt": ["get_doc"],
    "html": ["get_doc"],
    "xhtml": ["get_doc"],
    "tex": ["get_doc"],
    "md": ["get_doc"],
    "rst": ["get_doc"],
    "json": ["extract_json"],
    # Non-plain files
    "odt": ["get_doc"],
    "docx": ["get_doc"],
    "rtf": ["get_doc"],
    "pdf": ["extract_pdf", "get_doc"],
    "epub": ["get_doc"],
    # TODO: Add suport to DjVu
    # TODO: Add support to media files (audio, video, images)
}

# NOTE: Maps spaCy model and NLTK data langs for Tesseract
# TODO: Try another workaround that doesn't require mapping or that maps langs
#   instead (e.g. 'es': 'spa'); try to get rid of this constant
LANG_MAP = {
    "es_core_news_lg": "spa",
}
