from django.db import models
from django.utils.translation import gettext as _
from django_jsonform.models.fields import JSONField
from simple_history.models import HistoricalRecords
from multiselectfield import MultiSelectField
from ulid import ULID
from docs.fields import ULIDField
from docs.utils import (
    get_upload_path,
    get_schemata,
    get_list,
    get_installed_data,
    load_schema,
    process_file,
)
from docs.constants import METADATA_SCHEMA, SUPPORTED_SCHEMATA
from docs.validators import (
    validate_schema,
    validate_doc,
    validate_ext,
    validate_metadata,
    validate_metadata_schema,
)

VISIBILITY = (
    ("is_superuser", _("Superadministrador")),
    ("is_staff", _("Staff")),
    ("is_authenticated", _("Registrado")),
    ("is_anonymous", _("Anónimo")),
)


class Common(models.Model):
    created = models.DateTimeField(
        auto_now_add=True,
        editable=False,
        verbose_name=_("Añadido"),
    )
    last_modified = models.DateTimeField(
        auto_now=True,
        editable=False,
        verbose_name=_("Última modificación"),
    )
    history = HistoricalRecords(inherit=True)

    class Meta:
        abstract = True


class CommonNamed(Common):
    id = models.AutoField(
        primary_key=True,
        editable=False,
        verbose_name=_("ID"),
        help_text=_("Designación para uso interno."),
    )
    name = models.CharField(
        max_length=32,
        unique=True,
        verbose_name=_("Nombre"),
        help_text=_("""Designación para su selección al momento de añadir
                    un nuevo documento."""),
    )

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Doc(Common):
    id = ULIDField(
        default=ULID, primary_key=True, editable=False, verbose_name=_("ULID")
    )
    file = models.FileField(
        null=True,
        blank=True,
        upload_to=get_upload_path,
        max_length=None,
        validators=[validate_ext],
        verbose_name=_("Archivo"),
        help_text=_("Archivo en texto plano codificable a UTF-8."),
    )
    locale = models.ForeignKey(
        "Locale",
        on_delete=models.RESTRICT,
        verbose_name=_("Localización"),
        help_text=_("Localización para el procesamiento del documento."),
    )
    corpora = models.ManyToManyField(
        "Corpus",
        verbose_name=_("Corpora"),
        help_text=_("Uno o más corpus asociados al documento."),
    )
    metadata = JSONField(
        schema=get_schemata,
        blank=True,
        default=dict,
        verbose_name=_("Metadatos"),
        help_text=_("Metadatos del documento acorde a los corpora asociados."),
        validators=[validate_metadata],
    )
    valid_metadata = models.BooleanField(
        default=False,
        editable=False,
        verbose_name=_("Metadatos válidos"),
        help_text=_(
            "Indicación sobre el estado de validación de los metadatos."
        ),
    )
    valid_file = models.BooleanField(
        default=False,
        editable=False,
        verbose_name=_("Archivo válido"),
        help_text=_("Indicación sobre el estado de validación del archivo."),
    )

    class Meta:
        verbose_name = _("Documento")
        verbose_name_plural = _("Documentos")

    def __str__(self):
        return str(self.id)

    def save(self, *args, **kwargs):
        if self.metadata:
            self.valid_metadata = True
        else:
            self.valid_metadata = False
        if self.file:
            self.valid_file = True
        else:
            self.valid_file = False
        super(Doc, self).save(*args, **kwargs)
        process_file(self)

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude=exclude)
        validate_doc(self)


class Locale(CommonNamed):
    analyzer = models.SlugField(
        choices=get_installed_data(),
        verbose_name=_("Analizador"),
        help_text=_("""Modelos de lenguaje de spaCy o datos de NLTK disponibles
            para el análisis de los documentos.<br/>
            Si el modelo o el dato deseado no aparecen en el listado,
            contacta a la administración.<br/>
            Para más información, visita las documentaciones de
            <a href='https://spacy.io/models' target='blank'>spaCy</a> y
            <a href='https://www.nltk.org/data.html' target='blank'>NLTK</a>.
        """),
        default="",
    )
    schema = models.FileField(
        null=True,
        blank=True,
        upload_to=get_upload_path,
        max_length=None,
        validators=[validate_schema],
        verbose_name=_("Esquema"),
        help_text=_("""Archivo %s opcional para la validación de documentos que
        usen esta localización.""")
        % get_list(SUPPORTED_SCHEMATA, last_sep=_(" o ")),
    )
    schema_type = models.CharField(
        max_length=100,
        default="",
        editable=False,
        verbose_name=_("Tipo de esquema"),
        help_text=_("Indicación sobre el tipo de esquema."),
    )

    class Meta:
        verbose_name = _("Localización")
        verbose_name_plural = _("Localizaciones")

    def save(self, *args, **kwargs):
        # When there is a schema file, it double saves the first time because
        # the upload_to path uses self.id (self.pk) but this doesn't exist yet
        # Cfr. https://docs.djangoproject.com/en/5.1/ref/models/fields/#django.db.models.FileField.upload_to
        # Cfr. https://stackoverflow.com/a/15776267
        if self.schema and self.pk is None:
            saved_schema = self.schema
            self.schema = None
            super(Locale, self).save(*args, **kwargs)
            self.schema = saved_schema
        if self.schema:
            self.schema_type = load_schema(self.schema, rname=True)
        super(Locale, self).save(*args, **kwargs)


class Corpus(CommonNamed):
    # NOTE: It has no effect until you build on top of it
    visibility = MultiSelectField(
        choices=VISIBILITY,
        default=tuple([v[0] for v in VISIBILITY]),
        verbose_name=_("Visibilidad"),
        help_text=_("Uno o más tipos de usuario que pueden ver el corpus."),
    )
    metadata_schema = JSONField(
        schema=METADATA_SCHEMA,
        verbose_name=_("Esquema de metadatos"),
        help_text=_("Metadatos para los documentos asociados a este corpus."),
        validators=[validate_metadata_schema],
    )

    class Meta:
        verbose_name = _("Corpus")
        verbose_name_plural = _("Corpora")
        ordering = ["name"]  # noqa: RUF012
