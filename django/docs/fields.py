from django.core import exceptions
from django.db import models
from django.utils.translation import gettext as _
from docs import forms
from ulid import ULID


class ULIDField(models.Field):
    """
    Django model field type for handling ULID's.

    This field type is natively stored in the DB as a UUID (when supported)
    and a string/varchar otherwise.

    NOTE: Mod of https://github.com/ahawker/django-ulid/tree/master
          where ulid-py is replaced by python-ulid
    """

    description = "Universally Unique Lexicographically Sortable Identifier"
    empty_strings_allowed = False

    def __init__(self, verbose_name=None, **kwargs):
        kwargs["max_length"] = 26
        super().__init__(verbose_name, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        del kwargs["max_length"]
        return name, path, args, kwargs

    def get_internal_type(self):
        return "UUIDField"

    def get_db_prep_value(self, value, connection, prepared=False):
        if value is None:
            return None
        if not isinstance(value, ULID):
            value = self.to_python(value)
        if connection.features.has_native_uuid_field:
            return value.to_uuid()
        else:
            return value.hex

    def from_db_value(self, value, expression, connection):
        return self.to_python(value)

    def to_python(self, value):
        if value is None:
            return None
        try:
            return ULID.parse(value)
        except (AttributeError, ValueError):
            raise exceptions.ValidationError(
                _("'%(value)s' no es un ULID válido."),
                code="invalid",
                params={"value": value},
            ) from None

    def formfield(self, **kwargs):
        return super().formfield(
            **{
                "form_class": forms.ULIDField,
                **kwargs,
            }
        )
