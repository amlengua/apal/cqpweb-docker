from django.apps import apps
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django_jsonform.validators import JSONSchemaValidator
from lxml import etree
from pathlib import Path
from jsonschema.validators import Draft202012Validator
from docs.utils import load_schema, to_json_schema, flatten, get_meta, get_list
from docs.constants import SUPPORTED_FILES
import re
import json


def validate_ext(fieldfile):
    ext = Path(fieldfile.name).suffix[1:]
    exts = list(SUPPORTED_FILES.keys())
    if ext not in exts:
        raise ValidationError(
            _("""Archivo '%(file)s' no tiene una extensión válida. Extensiones
              válidas: %(exts)s."""),
            code="invalid_extension",
            params={
                "file": fieldfile.name,
                "exts": get_list(exts, last_sep=_(" o ")),
            },
        ) from None


def validate_schema(fieldfile):
    try:
        load_schema(fieldfile.file)
    except Exception:
        raise ValidationError(
            _("""Archivo '%(file)s' no es un esquema válido.
              Revisa y valida su sintaxis."""),
            code="invalid_schema",
            params={
                "file": fieldfile.name,
            },
        ) from None


def validate_doc(doc):
    errors = []
    # Schema validation
    if doc and doc.file and doc.locale_id and doc.locale.schema:
        try:
            schema = load_schema(doc.locale.schema.file)
            # When it is an XML Schema
            if schema:
                file = etree.parse(doc.file.open())
                if not schema.validate(file):
                    for err in schema.error_log:
                        errors.append(  # noqa: PERF401
                            ValidationError(
                                _("""%(level)s: %(file)s[%(line)s,%(col)s]:
                                        %(type)s: %(msg)s"""),
                                code=err.type_name,
                                params={
                                    "level": err.level_name,
                                    "file": Path(err.filename).name,
                                    "line": err.line,
                                    "col": err.column,
                                    "type": err.type_name,
                                    "msg": err.message,
                                },
                            )
                        )
            # When it is a JSON Schema (load_schema returns None when is valid)
            else:
                schema = json.load(doc.locale.schema.file.open())
                file = json.load(doc.file.open())
                validator = Draft202012Validator(schema)
                for err in validator.iter_errors(file):
                    errors.append(  # noqa: PERF401
                        ValidationError(
                            _("ERROR: %(file)s[%(path)s]: %(msg)s"),
                            code="invalid_json_doc",
                            params={
                                "file": Path(doc.file.name).name,
                                "path": next(iter(err.path)),
                                "msg": err.message,
                            },
                        ),
                    )
        except (etree.XMLSyntaxError, json.JSONDecodeError) as err:
            errors.append(ValidationError(err))
    if len(errors) > 0:
        raise ValidationError(errors)


def validate_json_metadata(rawschema, data):
    try:
        schema = to_json_schema(rawschema)
        validator = JSONSchemaValidator(schema)
        validator(data)
        return True
    except Exception:
        return False


def validate_metadata(data, corpus_id=None):
    # TODO: Needs more validation; e.g. recursivity, uniqueItems validation...
    errors = []
    # Assignment depends on whether the call comes from an admin action (first
    #   choice) or model validator (second choice)
    data = {corpus_id: data} if corpus_id else data
    corpus_id = corpus_id if corpus_id else next(iter(data.keys()))
    return_bool = True if corpus_id else False
    corpus = apps.get_model("docs", "Corpus").objects.get(id=corpus_id)
    for rawk1, v1 in flatten(data[corpus_id]).items():
        meta = get_meta(to_json_schema(corpus.metadata_schema), rawk1, v1)
        keys = meta.keys()
        # Validates constrain
        if "constrain" in keys and meta["constrain"]["const"]:
            constrain = meta["constrain"]["const"]
            # NOTE: Forces whole string match
            rgx = f"{constrain}$" if constrain[-1] != "$" else constrain
            if not re.compile(rgx).match(v1):
                errors.append(
                    ValidationError(
                        _("""%(key)s: '%(val)s' no coincide con la restricción
                          '%(rgx)s'."""),
                        code="regex_mismatch",
                        params={
                            "key": rawk1[-1],
                            "val": v1,
                            "rgx": rgx,
                        },
                    )
                )
        # Validates ID mobj
        if "systype" in keys and meta["systype"]["const"] == "string_id":
            emsg = _("%(key)s: '%(val)s' tiene %(ref)s de %(len)s caracteres.")
            if "minimum" in meta.keys() and len(v1) < meta["minimum"]:
                errors.append(
                    ValidationError(
                        emsg,
                        code="wrong_length",
                        params={
                            "key": rawk1[-1],
                            "val": v1,
                            "ref": _("menos"),
                            "len": meta["minimum"],
                        },
                    )
                )
            if "maximum" in meta.keys() and len(v1) < meta["maximum"]:
                errors.append(
                    ValidationError(
                        emsg,
                        code="wrong_length",
                        params={
                            "key": rawk1[-1],
                            "val": v1,
                            "ref": _("más"),
                            "len": meta["maximum"],
                        },
                    )
                )
            if re.search(r"\s", v1):
                errors.append(
                    ValidationError(
                        _("%(key)s: '%(val)s' no puede contener espacios."),
                        code="contains_spaces",
                        params={
                            "key": rawk1[-1],
                            "val": v1,
                        },
                    )
                )
    if errors and return_bool:
        return False
    elif errors:
        raise ValidationError(errors)
    elif return_bool:
        return True
    else:
        return data


def validate_metadata_schema(data, skip=False):
    errors = []
    for item in data["items"]:
        keys = item.keys()
        if item["systype"] == "list":
            inner_validation = validate_metadata_schema(item, skip=True)
            if isinstance(inner_validation, list):
                errors = errors + inner_validation
        # Validates min max range
        if "minimum" in keys or "minItems" in keys or "min_items" in keys:
            rgx1 = r"(imum|Items|_items)"
            rgx2 = r"min(I|_i)tems"
            imin = next(k for k in keys if re.match(r"min" + rgx1, k))
            imax = next(k for k in keys if re.match(r"max" + rgx1, k))
            minimum = item[imin] if item[imin] else 0
            maximum = item[imax] if item[imax] else 0
            if (minimum == 0 and re.match(rgx2, imin)) or maximum == 0:
                ref = _("mínimo") if minimum == 0 else _("máximo")
                errors.append(
                    ValidationError(
                        _("%(key)s: rango %(ref)s no puede ser 0."),
                        code="invalid_zero_value",
                        params={
                            "key": item["title"],
                            "ref": ref,
                        },
                    )
                )
            if minimum < -1 or maximum < -1:
                ref = _("mínimo") if minimum < -1 else _("máximo")
                errors.append(
                    ValidationError(
                        _("%(key)s: rango %(ref)s no puede ser menor a -1."),
                        code="invalid_negative_value",
                        params={
                            "key": item["title"],
                            "ref": ref,
                        },
                    )
                )
            if minimum > maximum > 0:
                errors.append(
                    ValidationError(
                        _("""%(key)s: rango máximo no puede ser menor al rango
                          mínimo."""),
                        code="invalid_max_value",
                        params={
                            "key": item["title"],
                        },
                    )
                )
        # Validates constrain RegEx syntax
        if "constrain" in keys and item["constrain"]:
            try:
                re.compile(item["constrain"])
            except Exception as err:
                errors.append(
                    ValidationError(
                        _("""%(key)s: restricción '%(rgx)s' es una expresión
                          regular inválida: %(err)s."""),
                        code="regex_syntax_error",
                        params={
                            "key": item["title"],
                            "rgx": item["constrain"],
                            "err": str(err),
                        },
                    )
                )
    if errors and skip:
        return errors
    elif errors:
        raise ValidationError(errors)
    else:
        return data
