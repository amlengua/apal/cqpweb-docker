from django.utils.translation import gettext_lazy as _
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse, Http404
from django.apps import apps
from django.db.models import FileField
from pathlib import Path


@staff_member_required
def doc(request, app, model, id, version, ext):
    url = Path(request.get_full_path())
    err = Http404(_("Archivo inexistente: '%s'") % url)
    try:
        model = apps.get_model(app, model)
        instance = model.objects.get(id=id)
    except Exception:
        raise err
    for filefield in instance._meta.get_fields():
        if not isinstance(filefield, FileField):
            continue
        file_path = Path(getattr(instance, filefield.name).path)
        file_url = ".".join(url.parts[-2:]).lower()
        if file_path.exists() and file_url == file_path.name.lower():
            contentd = 'attachment; filename="%s"' % file_path.name
            response = HttpResponse(file_path.read_bytes())
            response["Content-Disposition"] = contentd
            return response
    raise err
