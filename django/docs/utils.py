from django.utils.translation import gettext_lazy as _
from django.utils.safestring import mark_safe
from spacy.util import get_installed_models
from jsonschema.protocols import Validator
from lxml import etree, isoschematron
from multiprocessing import Process
from django.urls import reverse
from django.apps import apps
from pypdf import PdfReader
from pandoc.types import Pandoc, Meta, Para, Str, Span
from ocrmypdf import ocr
from pathlib import Path
from ulid import ULID
import pandoc
import json
import re


def get_upload_path(instance, filename, restamp=False):
    """Sets item upload path to MEDIA_ROOT/uploads/MODEL/ID/TIMESTAMP.EXT

    MODEL is the directory where the files of an specific model are stored.

    ID is the directory for the uploaded files of the same model instance.

    TIMESTAMP is the new file name for each uploaded file.

    EXT is the file extension (if any).

    This order allows grouping by model and instance, and it also allows
    sorting based on the datetime upload.
    """
    app = instance._meta.app_label
    model = instance._meta.model_name
    ext = Path(filename).suffix
    if isinstance(instance.id, ULID):
        id = ULID.parse(instance.id)
        timestamp = ULID().milliseconds if restamp else id.milliseconds
    else:
        id = instance.id
        timestamp = ULID().milliseconds
    path = "uploads/%s/%s/%s/%s%s" % (app, model, id, timestamp, ext)
    if hasattr(instance, "file") and instance.file.storage.exists(path):
        return get_upload_path(instance, filename, restamp=True)
    else:
        return path


def get_schemata(instance=None):
    schema = {"type": "object", "properties": {}}
    if not instance:
        return schema
    for corpus in instance.corpora.all():
        mschema = to_json_schema(corpus.metadata_schema)
        name = {
            "corpus": {
                "type": "string",
                "default": corpus.name,
                "readonly": True,
            }
        }
        keys = "keys" if "keys" in mschema else "properties"
        mschema[keys] = name | mschema[keys]
        schema["properties"].setdefault(str(corpus.id), mschema)
    return schema


def get_url(obj, as_html=True):
    url = reverse(
        "admin:%s_%s_change" % (obj._meta.app_label, obj._meta.model_name),
        args=[obj.id],
    )
    if as_html:
        return "<a href='%s'>%s</a>" % (url, obj.id)
    return url


def get_file_links(obj, attr, ext=None):
    fieldfile = getattr(obj, attr, None)
    if not fieldfile:
        return None
    html = "<a href='/%s'>%s</a>"
    links = []
    directory = Path(fieldfile.path).parent
    version = Path(fieldfile.name).stem
    # Uses glob() because the file extension could be unknown
    path = list(directory.glob("%s.*" % version))[0]
    if path:
        parent = Path(*Path(fieldfile.url).parts[2:-1])
        url = parent / version / path.suffix[1:].lower()
        # TODO: Append editor app link (parent / version, _("Editar")) before
        ext = ext if ext else url.name
        links.append(html % (url, ext.upper()))
    return mark_safe(get_list(links, last_sep=_(" y ")))


def get_meta(schema, rawk1, v1):
    meta = {}
    k1 = [k for k in rawk1 if isinstance(k, str)]
    for rawk2, v2 in flatten(schema["properties"]).items():
        k2 = list(rawk2)
        if set(k1).issubset(set(k2)):
            copy1 = k1[:]
            copy2 = k2[:]
            while copy1 and copy2:
                if copy1[0] == copy2[0]:
                    copy1.pop(0)
                copy2.pop(0)
            meta.setdefault(tuple(copy2), v2)
    return unflatten(meta)


def get_mobj(systype, name, **kwargs):
    mobj = {
        "systype": {
            "const": systype,
            "widget": "hidden",
        },
        "title": {
            "title": _("Nombre"),
            "type": "string",
            "required": True,
            "helpText": _("Nombre necesario del metadato."),
        },
        "default": {
            "title": _("Valor por defecto"),
            "type": "string",
            "required": False,
            "helpText": _(
                "Valor opcional cuando el usuario no introduce información."
            ),
        },
        "constrain": {
            "title": _("Restricción"),
            "type": "string",
            "required": False,
            "helpText": _(
                """Expresión regular que debe coincidir con el valor
                introducido."""
            ),
        },
        "required": {
            "title": _("Requerido"),
            "type": "boolean",
            "helpText": _(
                "De marcarse, el valor del metadato no puede dejarse vacío."
            ),
        },
        "uniqueItems": {
            "title": _("Ítems únicos"),
            "type": "boolean",
            "helpText": _("De marcarse, impide elementos repetidos."),
        },
    }

    if systype not in ["string", "string_id"]:
        del mobj["constrain"]

    if systype in ["list", "object"]:
        del mobj["default"]

    if systype not in ["list", "object"]:
        del mobj["uniqueItems"]

    # default__boolean, title__helpText, other...
    for rawkeys in kwargs.keys():  # noqa: PLC0206
        keys = rawkeys.split("__")
        mobj.setdefault(keys[0], None)
        if len(keys) == 1:
            mobj[keys[0]] = kwargs[rawkeys]
        else:
            mobj[keys[0]].setdefault(keys[1], None)
            mobj[keys[0]][keys[1]] = kwargs[rawkeys]

    return {
        "type": "object",
        "title": name,
        "properties": mobj,
    }


def get_list(rawlist, last_sep=", ", sep=", "):
    if isinstance(rawlist, list):
        alist = rawlist
    elif isinstance(rawlist, dict):
        alist = [str(e) for e in rawlist.values()]
    else:
        raise TypeError("'%s' is not a list or dict" % str(rawlist))
    if len(alist) > 1:
        return sep.join(alist[:-1]) + last_sep + alist[-1]
    else:
        return sep.join(alist)


def get_installed_data():
    spacy_models = [(e, f"{e} (spaCy)") for e in get_installed_models()]
    nltk_data = []  # TODO: Implement the use of NLTK besides spaCy
    return spacy_models + nltk_data


def to_json_schema(rawschema):
    schema = {}
    for rawitem in rawschema["items"]:
        newitem = {"type": rawitem["systype"].split("_")[0]}
        for key, val in rawitem.items():
            newval = None
            # For number, if is an integer, the type is changed
            if key == "numtype" and val == _("Entero"):
                newitem["type"] = "integer"
            # For dateime, date and time, the correct formatting is added
            if val in ["string_datetime", "string_date", "string_time"]:
                newitem["format"] = val.split("_")[-1]
            # For booleans, the value gets converted to True or False
            if newitem["type"] == "boolean" and key == "default":
                newval = val == _("Verdadero")
            # For lists, the value gets coverted with recursion
            if newitem["type"] == "list" and isinstance(val, list):
                newval = to_json_schema({"items": val})
            if newitem["type"] == "object" and key == "properties":
                newval = to_json_obj(val)
            # Avoids required value to be None
            if key == "required":
                newval = True if val else False
            # Adds item key to schema
            if key == "title":
                schema.setdefault(val, {})
            if key in ["constrain", "systype"]:
                newval = {"const": val, "widget": "hidden"}
            # Avoids adding unwanted keys
            # 'type' here means 'user type' not the JSON Schema 'type' key
            if (key in ["numtype", "type"] and val) or (
                re.match(r"m(in|ax)(imum|Items|_items)", key)
                and (not val or val < 0)
            ):
                continue
            newitem[key] = newval if newval is not None else val
        schema[list(schema.keys())[-1]] = newitem
    return {
        "type": "object",
        "properties": schema,
    }


def to_json_obj(ls):
    ls = list(to_json_schema({"items": ls})["properties"].values())
    for i, obj in enumerate(ls):
        ls[i] = {
            "type": "object",
            "title": obj["title"],
            "properties": {
                "id": {"const": f"item_{i}", "widget": "hidden"},
                "value": {
                    "title": _("Valor"),
                    **{k: v for k, v in obj.items() if k != "title"},
                },
            },
        }
    return {
        "items": {
            "title": _("Ítems"),
            "type": "list",
            "items": {"anyOf": ls},
        }
    }


def load_schema(fileobj, rname=False):
    """Tries to load schema; throws error when it can't"""
    from docs.constants import SUPPORTED_SCHEMATA

    schemata = list(SUPPORTED_SCHEMATA.keys())
    for parser, obj, name in [
        ((etree, "parse"), etree, schemata[0]),
        (False, etree, schemata[1]),
        ((json, "load"), Validator, schemata[2]),
        ((etree, "parse"), etree, schemata[3]),
        ((etree, "parse"), etree, schemata[4]),  # Pre-ISO-Schematron
        ((etree, "parse"), isoschematron, schemata[4]),  # ISO-Schematron
    ]:
        try:
            file = fileobj.open()
            parsed = getattr(*parser)(file) if parser else file
            schema = getattr(obj, name)(parsed)
            return SUPPORTED_SCHEMATA[name] if rname else schema
        # NOTE: By trying is how it is possible to arbitrary load the schema
        except Exception:  # noqa: PERF203
            pass
    raise RuntimeError(
        _("Archivo '%s' no es un esquema válido. Revisa y valida su sintaxis.")
        % fileobj
    )


def pipeline_extract_json(obj):
    blocks = []
    for keys, val in flatten(json.load(obj.file.open())).items():
        inlines, spanval = [], []
        spankeys = [Span({"type": type(k).__name__}, Str(k)) for k in keys]
        spanval.append(Span({"type": type(val).__name__}, Str(val)))
        inlines.append(Span({"type": "dict_keys"}, spankeys))
        inlines.append(Span({"type": "dict_values"}, spanval))
        blocks.append(Para(inlines))
    return Pandoc(Meta({}), blocks)


def pipeline_extract_pdf(obj):
    path = obj if isinstance(obj, (str, Path)) else obj.file.path
    reader = PdfReader(path)
    blocks = [p.extract_text() for p in reader.pages]
    text = "\n\n".join(blocks)
    if not text:
        # TODO: Try to get rid of LANG_MAP and instead use a function
        from docs.constants import LANG_MAP

        locale = apps.get_model("docs", "Locale").objects.get(id=obj.locale_id)
        lang_a3 = LANG_MAP[locale.model]
        ipath = obj.file.path
        opath = Path(ipath).parent / ("%s.ocr.pdf" % Path(ipath).stem)
        p = Process(
            target=ocr, args=[ipath, opath], kwargs={"language": lang_a3}
        )
        p.start()
        p.join()
        if p.exitcode == 0:
            return pipeline_extract_pdf(opath)
    return text


def pipeline_get_doc(obj):
    if isinstance(obj, str):
        return pandoc.read(obj)
    else:
        return pandoc.read(file=obj.file.path)


def process_file(instance):
    if not instance.file:
        return
    from docs.constants import SUPPORTED_FILES

    obj = instance
    ext = Path(instance.file.name).suffix[1:]
    path = Path(instance.file.path).parent / Path(instance.file.name).stem
    # NOTE: All pipelines must end with a Pandoc obj
    for pipeline in SUPPORTED_FILES[ext]:
        obj = globals()["pipeline_%s" % pipeline](obj)
    # TODO: Try to use this file + django-jsonform for editor app
    pandoc.write(obj, file=path, format="json")


def decapitalize(string):
    if len(string) > 1:
        return string[0].lower() + string[1:]
    else:
        return string.lower()


def flatten(rawobj, parents=()):
    res = {}
    if not isinstance(rawobj, (dict, list)):
        msg = "Wrong type '%s'; expecting 'dict' or 'list'" % type(rawobj)
        raise TypeError(msg)
    obj = rawobj.items() if isinstance(rawobj, dict) else enumerate(rawobj)
    for rawkey, val in obj:
        key = (*list(parents), rawkey)
        if isinstance(val, (dict, list)):
            res = res | flatten(val, key)
        else:
            res.setdefault(key, val)
    return res


def unflatten(rawobj):
    res, obj = {}, {}
    if not isinstance(rawobj, dict):
        raise TypeError("Wrong type '%s'; expecting 'dict'" % type(rawobj))
    for keys, val in rawobj.items():
        if not isinstance(keys, tuple):
            raise TypeError("Wrong type '%s'; expecting 'tuple'" % type(keys))
        enum = list(enumerate(keys))
        for i, key in enum:
            if i == 0 and len(enum) == 1:
                res[key] = val
            elif i == 0:
                res.setdefault(key, {})
                obj = res[key]
            elif (i, key) != enum[-1]:
                obj.setdefault(key, {})
                obj = obj[key]
            else:
                obj[key] = val
    return res
