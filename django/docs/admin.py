from django.contrib import admin
from django.forms import CheckboxSelectMultiple
from django.db import models
from django.utils.translation import gettext_lazy as _
from simple_history.admin import SimpleHistoryAdmin
from django_jsonform.models.fields import JSONField
from django_jsonform.forms.fields import JSONFormWidget
from docs.models import Doc, Locale, Corpus
from docs.utils import get_schemata, get_file_links
from docs.actions import delete_bulk, validate_metadatas, validate_files
from pathlib import Path

# ruff: noqa: RUF012

# NOTE: Default 'delete_selected' disabled in favor of 'delete_bulk';
#   'delete_bulk' is a clone of 'delete_selected' with the mod that doesn't
#   allow deletion if the object is in a many-to-many relationship
admin.site.disable_action("delete_selected")
admin.site.add_action(delete_bulk)


@admin.register(Doc)
class DocAdmin(SimpleHistoryAdmin):
    list_display = [
        "id",
        "file_version",
        "file_link",
        "locale",
        "corpora_list",
        "valid_metadata",
        "valid_file",
        "last_modified",
        "created",
    ]
    list_filter = ["corpora", "created", "last_modified"]
    search_fields = ["id", "corpora__name"]
    actions = [validate_metadatas, validate_files]
    filter_horizontal = ("corpora",)
    ordering = ["-last_modified"]
    formfield_overrides = {
        models.ManyToManyField: {"widget": CheckboxSelectMultiple},
        JSONField: {
            "widget": JSONFormWidget(
                schema=get_schemata, validate_on_submit=True
            )
        },
    }

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if obj:
            form.base_fields["metadata"].widget = JSONFormWidget(
                schema=get_schemata(obj), validate_on_submit=True
            )
        return form

    @admin.display(description=_("Versión"), ordering="file")
    def file_version(self, obj):
        path = Path(obj.file.name).with_suffix("").stem
        return path if path else None

    @admin.display(description=_("Archivo"), ordering="file")
    def file_link(self, obj):
        return get_file_links(obj, "file")

    @admin.display(
        description=_("Corpora"),
        # TODO: Buggy, duplicates one entry when at least one doc has more than
        #   one corpus; but it works
        ordering="corpora__name",
    )
    def corpora_list(self, obj):
        return sorted(obj.corpora.values_list("name", flat=True))


@admin.register(Locale)
class LocaleAdmin(SimpleHistoryAdmin):
    list_display = [
        "id",
        "name",
        "file_link",
        "model",
        "created",
        "last_modified",
    ]
    list_filter = ["analyzer", "created", "last_modified"]
    search_fields = ["id", "name", "model"]
    ordering = ["-last_modified"]

    @admin.display(description=_("Esquema"), ordering="schema")
    def file_link(self, obj):
        return get_file_links(obj, "schema", ext=obj.schema_type)


@admin.register(Corpus)
class CorpusAdmin(SimpleHistoryAdmin):
    list_display = ["id", "name", "created", "last_modified"]
    list_filter = ["created", "last_modified"]
    search_fields = ["id", "name"]
    ordering = ["-last_modified"]
