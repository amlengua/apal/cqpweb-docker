from django import forms
from django.core import exceptions
from django.utils.translation import gettext as _
from ulid import ULID


class ULIDField(forms.CharField):
    """
    Django form field type for handling ULID values.

    NOTE: Mod of https://github.com/ahawker/django-ulid/tree/master
          where ulid-py is replaced by python-ulid
    """

    def prepare_value(self, value):
        return str(ULID.parse(value)) if value is not None else ""

    def to_python(self, value):
        value = super().to_python(value)

        if value in self.empty_values:
            return None
        try:
            return ULID.parse(value)
        except (AttributeError, ValueError):
            raise exceptions.ValidationError(
                _("Ingresa un ULID válido."), code="invalid"
            ) from None
