"""
URL configuration for DCWB project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from importlib import find_loader
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path, re_path
from home.views import error_400, error_403, error_404, error_500
from docs import views

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("home.urls")),
    path("search/", include("search.urls")),
    path("api/", include("api.urls")),
    # NOTE: All apps should use this path if the HttpResponse is a file
    path(
        "uploads/<str:app>/<str:model>/<str:id>/<str:version>/<str:ext>",
        views.doc,
    ),
    # Adds static paths
    *static(settings.STATIC_URL, document_root=settings.STATIC_ROOT),
    # Adds apps urls if they have an urls.py file
    *[
        path("%s/" % app, include("%s.urls" % app))
        for app in settings.INSTALLED_APPS
        if (settings.BASE_DIR / app).is_dir() and find_loader("%s.urls" % app)
    ],
]

if "rosetta" in settings.INSTALLED_APPS:
    urlpatterns += [re_path(r"^locale/", include("rosetta.urls"))]

handler400 = error_400
handler403 = error_403
handler404 = error_404
handler500 = error_500
