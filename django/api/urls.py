from django.urls import path

from . import views

urlpatterns = [
    path("search/", views.search, name="api-search"),
    path("info/", views.info, name="api-info"),
    path("about/", views.about, name="api-about"),
]
