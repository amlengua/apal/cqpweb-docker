import re
import csv
import json
from collections.abc import MutableMapping
from django.http import HttpResponse, JsonResponse, QueryDict


# NOTE: Valid API outputs; first element is default
OUTPUTS = ["json", "csv"]


class PseudoBuffer:
    """Writes CSV files

    Cfr. “Streaming large CSV files” in
    https://docs.djangoproject.com/en/4.2/howto/outputting-csv/
    """

    def write(self, value):
        return value


class CsvResponse(HttpResponse):
    """Implements a CSV response

    Cfr. https://docs.djangoproject.com/en/2.2/_modules/django/http/response/
    """

    def __init__(self, data, filename="file.csv"):
        rows, writer = data_rows(data), csv.writer(PseudoBuffer())
        kwargs = {
            "content_type": "text/csv",
            "headers": {
                "Content-Disposition": f'attachment; filename="{filename}"'
            },
        }
        super().__init__((writer.writerow(row) for row in rows), **kwargs)


def get_querydict(request):
    querydict = QueryDict("", mutable=True)
    if request.GET:
        querydict.update(request.GET)
    else:
        try:
            querydict.update(json.loads(request.body.decode("utf-8")))
        except Exception:
            pass
    if "output" not in querydict or querydict["output"] not in OUTPUTS:
        querydict["output"] = OUTPUTS[0]
    return querydict


def get_response(res, output, filename):
    if output == "json":
        return JsonResponse(data_json(res, serialize=False))
    else:
        return CsvResponse(res, filename)


def flatten(dictionary, parent_key="", sep="."):
    """
    From:
        https://www.freecodecamp.org/news/how-to-flatten-a-dictionary-in-python-in-4-different-ways/
    """
    items = []
    for k, v in dictionary.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        elif isinstance(v, list):
            for i, e in enumerate(v):
                inner_key = new_key + sep + str(i)
                if isinstance(e, MutableMapping):
                    items.extend(flatten(e, inner_key, sep=sep).items())
                else:
                    items.append((inner_key, e))
        else:
            items.append((new_key, v))
    return dict(items)


def data_rows(data, stringify=True):
    out, data = [], data_json(data, serialize=False)
    if "concordance" in data.keys():
        out.append("index doc before match after".split(" "))
        for i, item in enumerate(data["concordance"]):
            key = data["meta"]["result"]["default_p"]
            index = data["meta"]["result"]["items_index"] + i
            before = " ".join([str(t[key]) for t in item["left"]])
            match = " ".join([str(t[key]) for t in item["center"]])
            after = " ".join([str(t[key]) for t in item["right"]])
            out.append([index, item["info"]["id"], before, match, after])
    else:
        data = flatten(data)
        out.append(data.keys())
        out.append(data.values())
    for i, row in enumerate(out):
        out[i] = [str(cl) for cl in row] if stringify else list(row)
    return out


def data_json(data, serialize=True):
    if not isinstance(data, dict):
        data = {"res": data}
    data = retype(data)
    if serialize:
        return json.dumps(data, indent=2)
    else:
        return data


def retype(obj):
    rgx_num = r"[-\+]{0,1}\d+(\.\d+){0,1}"  # NOTE: Floats must have period
    if isinstance(obj, dict):
        obj = {key: retype(val) for key, val in obj.items()}
    elif isinstance(obj, list):
        obj = [retype(el) for el in obj]
    else:
        obj = str(obj).strip()
        tmp = obj.replace(",", "")
        if obj:
            quotes = obj[0] + obj[-1]
            obj = obj[1:-1] if quotes in ["''", '""'] else obj
        if re.fullmatch(rgx_num, tmp):
            try:
                obj = float(tmp) if "." in tmp else int(tmp)
            except Exception:
                pass
        obj = True if str(obj).lower() == "true" else obj
        obj = False if str(obj).lower() == "false" else obj
        obj = None if str(obj).lower() in ["none", "null"] else obj
    return obj
