from handlers import cwb
from api.utils import get_querydict, get_response
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

# TODO: Rewrite it using https://www.django-rest-framework.org/


@method_decorator(csrf_exempt, name="dispatch")
def search(request):
    """Search query in corpus

    Call example for JSON:
        curl URL/api/search/ \
        -d '{"corpus": "_test1", "query": "*o*", "output": "json"}'

    Call example for CSV:
        curl URL/api/search/ \
        -d '{"corpus": "_test1", "query": "*o*", "output": "csv"}'
    """
    querydict = get_querydict(request)
    res = cwb.search(querydict, request.user)
    filename = f"result_page{res['meta']['search']['page']}.csv"
    return get_response(res, querydict["output"], filename)


@method_decorator(csrf_exempt, name="dispatch")
def info(request):
    """Gives corpus info

    Call example for JSON:
        curl URL/api/info/ -d '{"corpus": "_test1", "output": "json"}'

    Call example for CSV:
        curl URL/api/info/ -d '{"corpus": "_test1", "output": "csv"}'
    """
    querydict = get_querydict(request)
    if "corpus" not in querydict:
        querydict["corpus"] = ""
    res = cwb.all_info(querydict["corpus"])
    filename = f"{querydict['corpus']}_info.csv"
    return get_response(res, querydict["output"], filename)


@method_decorator(csrf_exempt, name="dispatch")
def about(request):
    """Gives web app info

    Call example for JSON:
        curl URL/api/about/ -d '{"output": "json"}'

    Call example for CSV:
        curl URL/api/about/ -d '{"output": "csv"}'
    """
    querydict = get_querydict(request)
    res = cwb.about()
    filename = "about.csv"
    return get_response(res, querydict["output"], filename)
