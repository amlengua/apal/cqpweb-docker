import csv
import json
from api import utils
from io import StringIO
from handlers import cwb
from django.test import Client, RequestFactory
from django.http import QueryDict, JsonResponse
from tests.common import get_asset


def assert_call(url, query, method, mkey="qdict", isjson=True):
    c, fn, qdict = Client(), getattr(cwb, method), QueryDict(query, True)
    res1 = c.get(f"{url}?{query}")
    res2 = c.get(url, data=qdict.dict())
    res3 = fn(qdict) if mkey == "qdict" else fn(qdict[mkey]) if mkey else fn()
    txt1, txt2 = res1.content.decode(), res2.content.decode()
    txt3 = json.dumps(utils.retype(res3)) if isjson else utils.data_rows(res3)
    if not isjson:
        txt1 = list(csv.reader(StringIO(txt1)))
        txt2 = list(csv.reader(StringIO(txt2)))
    assert res1.status_code == res2.status_code == 200  # noqa: PLR2004
    assert txt1 == txt2 == txt3


def test_api_search_json():
    url, query = "/api/search/", "corpus=_TEST1&query=*o*&output=json"
    assert_call(url, query, "search")


def test_api_search_csv():
    url, query = "/api/search/", "corpus=_TEST1&query=*o*&output=csv"
    assert_call(url, query, "search", isjson=False)


def test_api_info_json():
    url, query = "/api/info/", "corpus=_test1"
    assert_call(url, query, "all_info", mkey="corpus")


def test_api_info_csv():
    url, query = "/api/info/", "corpus=_test1&output=csv"
    assert_call(url, query, "all_info", mkey="corpus", isjson=False)


def test_api_about_json():
    url, query = "/api/about/", ""
    assert_call(url, query, "about", mkey="")


def test_api_about_csv():
    url, query = "/api/about/", "output=csv"
    assert_call(url, query, "about", mkey="", isjson=False)


def test_pseudobuffer():
    assert utils.PseudoBuffer().write("foo") == "foo"


def test_csvresponse():
    res = utils.CsvResponse({"key": "val"})
    assert res.status_code == 200  # noqa: PLR2004
    assert res.content == b"key\r\nval\r\n"
    assert res.headers == {
        "Content-Disposition": 'attachment; filename="file.csv"',
        "Content-Type": "text/csv",
    }


def test_get_querydict():
    req = RequestFactory().get(
        "/search/?corpus=_TEST1&query=Texto&mode=simple&page=1"
    )
    assert utils.get_querydict(req).dict() == {
        "corpus": "_TEST1",
        "query": "Texto",
        "mode": "simple",
        "page": "1",
        "output": "json",
    }


def test_get_response():
    c = Client()
    res = c.get("")
    for output, obj in [("json", JsonResponse), ("csv", utils.CsvResponse)]:
        assert isinstance(utils.get_response(res, output, "file.name"), obj)


def test_flatten():
    for obj in [
        ({}, {}),
        ({"a": 0}, {"a": 0}),
        ({"a": 0, "b": {"c": 1}}, {"a": 0, "b.c": 1}),
        ({"a": {"b": {"c": 0}}}, {"a.b.c": 0}),
        ({"a": {"b": {"c": 0}, "d": 1}}, {"a.b.c": 0, "a.d": 1}),
        ({"a": [9, 8, 7]}, {"a.0": 9, "a.1": 8, "a.2": 7}),
        ({"a": {"b": [9, 8, 7]}}, {"a.b.0": 9, "a.b.1": 8, "a.b.2": 7}),
        ({"a": {"b": [9, 8]}, "c": 7}, {"a.b.0": 9, "a.b.1": 8, "c": 7}),
        ({"a": {"b": [9, 8]}, "c": [7]}, {"a.b.0": 9, "a.b.1": 8, "c.0": 7}),
    ]:
        assert utils.flatten(obj[0]) == obj[1]


def test_data_rows():
    sample = get_asset(1)
    for obj in [
        (("", True), [["res"], [""]]),
        ((0, True), [["res"], ["0"]]),
        ((True, True), [["res"], ["True"]]),
        (("", False), [["res"], [""]]),
        ((0, False), [["res"], [0]]),
        ((True, False), [["res"], [True]]),
        (({"a": "", "b": {"c": 0}}, True), [["a", "b.c"], ["", "0"]]),
        (({"a": {"b": True}}, True), [["a.b"], ["True"]]),
        (({"a": "", "b": {"c": 0}}, False), [["a", "b.c"], ["", 0]]),
        (({"a": {"b": True}}, False), [["a.b"], [True]]),
        ((sample, True), get_asset(2)),
        ((sample, False), get_asset(3)),
    ]:
        assert utils.data_rows(*obj[0]) == obj[1]


def test_data_json():
    res1 = '{\n  "a": "",\n  "b": {\n    "c": 0\n  }\n}'
    res2 = '{\n  "a": {\n    "b": true\n  }\n}'
    for obj in [
        (("", True), '{\n  "res": ""\n}'),
        (("0", True), '{\n  "res": 0\n}'),
        (("true", True), '{\n  "res": true\n}'),
        (("", False), {"res": ""}),
        (("0", False), {"res": 0}),
        (("none", False), {"res": None}),
        (({"a": "", "b": {"c": "0"}}, True), res1),
        (({"a": {"b": "true"}}, True), res2),
        (({"a": "", "b": {"c": "1.0"}}, False), {"a": "", "b": {"c": 1.0}}),
        (({"a": {"b": "null"}}, False), {"a": {"b": None}}),
    ]:
        assert utils.data_json(*obj[0]) == obj[1]


def test_retype():
    for obj in [
        ([" foo ", "'bar'", '"baz"'], ["foo", "bar", "baz"]),
        ([0, "1", "02", 3.0, "4.0", "05.00"], [0, 1, 2, 3.0, 4.0, 5.0]),
        ([6000, "7,000", "08,000", "090,00"], [6000, 7000, 8000, 9000]),
        (["+0", "-1", "+02", "-03", "+04.0"], [0, -1, 2, -3, 4.0]),
        (["-.5", "+060", "-07,00", "+-8.00"], ["-.5", 60, -700, "+-8.00"]),
        (["true", "false", "none", "Null"], [True, False, None, None]),
        ({"a": " x ", "b": "'y'", "c": "0"}, {"a": "x", "b": "y", "c": 0}),
        ({"a": {"b": {"c": "0"}}, "d": "1.0"}, {"a": {"b": {"c": 0}}, "d": 1}),
    ]:
        assert utils.retype(obj[0]) == obj[1]
