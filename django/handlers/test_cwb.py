import pytest
import re
from handlers import cwb
from tests.common import assert_keys, assert_values, get_asset
from django.contrib.auth.models import AnonymousUser, User
from django.http import QueryDict
from django.conf import settings
from pathlib import Path

# NOTE: All tests implicitly tests cwb cqp()

search_keys = "corpus output query mode page status msg"
result_keys = """
default_p items_index abs_freq pmw_freq total_docs total_pages pagination docs
"""


def test_registry():
    assert Path("/usr/local/share/cwb/registry/").exists()


# NOTE: Implicitly tests cwb registry() info() and context_descriptor()
def test_all_info_success():
    res = cwb.all_info("_test1")
    assert_keys(res, "registry info context_descriptor")
    assert_values(res, dict, dict, dict)
    assert_keys(res["registry"], "name id home info attribute structure")
    assert_values(res["registry"], str, str, str, str, list, list)
    assert_keys(res["info"], "size charset properties")
    assert_values(res["info"], int, str, dict)
    assert_keys(
        res["context_descriptor"],
        "left_context",
        "right_context",
        "corpus_position",
        "target_anchors",
        "positional_attributes",
        "structural_attributes",
        "aligned_corpora",
    )
    assert_values(
        res["context_descriptor"], str, str, str, str, list, list, None
    )


# NOTE: Implicitly tests cwb registry() info() and context_descriptor()
def test_all_info_error():
    res = cwb.all_info("foo")
    assert_keys(res, "registry info context_descriptor")
    assert_values(res, {}, {}, dict)
    assert_keys(
        res["context_descriptor"],
        "left_context",
        "right_context",
        "corpus_position",
        "target_anchors",
        "positional_attributes",
        "structural_attributes",
        "aligned_corpora",
    )
    assert_values(
        res["context_descriptor"], str, str, str, str, None, None, None
    )


def test_about():
    assert cwb.about() == settings.DCWB


def test_corpora():
    res = cwb.corpora()
    assert_keys(res, "list")
    for item in res["list"]:
        assert_keys(item, "id_upper name id home info attribute structure")
        assert_values(item, str, str, str, str, str, list, list)


def test_search_success():
    meta_keys = "search result"
    search_vals = ["_TEST1", "*o*", "simple", "html", 1, True, str]
    result_vals = ["word", 1, 6, 285714.29, 1, 1, list, dict]
    res = cwb.search(QueryDict("corpus=_test1&query=*o*", mutable=True))
    assert_keys(res, "meta concordance")
    assert_keys(res["meta"], meta_keys)
    assert_keys(res["meta"]["search"], search_keys)
    assert_keys(res["meta"]["result"], result_keys)
    assert_values(res, dict, list)
    assert_values(res["meta"], dict, dict)
    assert_values(res["meta"]["search"], *search_vals)
    assert_values(res["meta"]["result"], *result_vals)
    for item in res["concordance"]:
        assert_keys(item, "info left center right")
        assert_values(item, dict, list, list, list)


def test_search_error():
    meta_keys = "search"
    search_vals = ["_TEST1", "foo", "simple", "html", 1, False, str]
    res = cwb.search(QueryDict("corpus=_test1&query=foo", mutable=True))
    assert_keys(res, "meta concordance")
    assert_keys(res["meta"], meta_keys)
    assert_keys(res["meta"]["search"], search_keys)
    assert_values(res, dict, list)
    assert_values(res["meta"], dict)
    assert_values(res["meta"]["search"], *search_vals)
    assert res["concordance"] == []


def test_validate_default():
    qdict = cwb.validate(QueryDict("", mutable=True))
    assert_values(qdict, "", "", "simple", "html", 1, False)
    assert_keys(qdict, "corpus query mode output page file msg status")


def test_validate_custom():
    query = "corpus=_test1&query=foo&mode=cqp&output=json&page=2"
    qdict = cwb.validate(QueryDict(query, mutable=True))
    assert_values(qdict, "_TEST1", "foo", "cqp", "json", 2, True, str)


def test_validate_msgs():
    queries_msgs = (
        ("", "Se requiere un corpus y al menos un término de", False),
        ("corpus=foo", "Se requiere un corpus y al menos un ", False),
        ("query=foo", "Se requiere indicar un corpus válido.", False),
        ("corpus=_test1", "Se requiere al menos un término d", False),
        ("corpus=_test1&query=foo", "Término de búsqueda vál", True),
    )
    for query, msg, status in queries_msgs:
        qdict = cwb.validate(QueryDict(query, mutable=True))
        assert msg in qdict["msg"]
        assert status == qdict["status"]


def test_sanitize():
    for obj in [
        (("*", "simple"), ""),
        ((" query ", "simple"), '"query"%cd'),
        (("query;", "simple"), '"query"%cd'),
        (("query*", "simple"), '"query.*"%cd'),
        (("*query*", "simple"), '".*query.*"%cd'),
        (("*query", "simple"), '".*query"%cd'),
        (('q_a "q_b"', "simple"), '"q_a"%cd "q_b"'),
        (('"q_b" q_c;', "simple"), '"q_b" "q_c"%cd'),
        (('q_a "q_b" q_c;', "simple"), '"q_a"%cd "q_b" "q_c"%cd'),
        (("query", "cqp"), "query"),
        (("  query  ", "cqp"), "query"),
        (("query;", "cqp"), "query"),
        (("query*", "cqp"), "query*"),
        (('q_a "q_b" q_c;', "cqp"), 'q_a "q_b" q_c'),
        (("set Context\n3; show +word;", "cqp"), "Context 3; +word"),
    ]:
        assert cwb.sanitize(*obj[0]) == obj[1]


def test_requery():
    user = {"LEFT": "3 words", "RIGHT": "3 words"}
    query = """
        _test1; set pm "sgml"; set ps "text_id"; set AttributeSeparator "\t";
        set LeftContext 3 words; set RightContext 3 words; show +word;
        show +FORM; show +LEMMA; show +TAG; show +SHORT_TAG; show +MSD;
        show +NEC; show +SENSE; show +SYNTAX; show +DEPHEAD; show +DEPREL;
        show +COREF; show +TOKENID; a_query within s;
    """
    res1 = re.sub(r"[\n ]+", " ", query.strip())
    res2 = re.sub(r"show \+\w+; ", "", res1)
    res3 = re.sub(r"^\w+", "", res2)
    for obj in [
        (("a_query", "_test1", cwb.context_descriptor("_test1"), user), res1),
        (("a_query", "_test1", cwb.context_descriptor(), user), res2),
        (("a_query", "", cwb.context_descriptor(), user), res3),
    ]:
        assert cwb.requery(*obj[0]) == obj[1]


@pytest.mark.django_db
def test_grant():
    user1 = User.objects.create_user("foo", "password", is_staff=False)
    user2 = User.objects.create_user("bar", "password", is_staff=True)
    admin = User.objects.create_superuser("baz", "password")
    for obj in [
        ((AnonymousUser(), "context"), {"BOTH": "3 words"}),
        ((user1, "context"), {"LEFT": "5 words", "RIGHT": "5 words"}),
        ((user2, "context"), {"LEFT": "1 sentences", "RIGHT": "1 sentences"}),
        ((admin, "context"), {"LEFT": "1 sentences", "RIGHT": "1 sentences"}),
    ]:
        assert cwb.grant(*obj[0]) == obj[1]


def test_contextualize():
    anon_context = cwb.contextualize(cwb.grant(AnonymousUser(), "context"))
    for obj in [
        ({}, anon_context),
        ({"LEFT": "3 words"}, anon_context),
        ({"RIGHT": "3 words"}, anon_context),
        ({"FOO": "3 words"}, anon_context),
        ({"BOTH": "3 words"}, {"LEFT": "3 words", "RIGHT": "3 words"}),
        ({"BOTH": "5 words", "A": 0}, {"LEFT": "5 words", "RIGHT": "5 words"}),
        ({"BOTH": "3 sentences"}, {"LEFT": "3 s", "RIGHT": "3 s"}),
        (
            {"LEFT": "5 w", "RIGHT": "5 words"},
            {"LEFT": anon_context["LEFT"], "RIGHT": "5 words"},
        ),
        (
            {"LEFT": "5 words", "RIGHT": "5 w"},
            {"LEFT": "5 words", "RIGHT": anon_context["RIGHT"]},
        ),
        (
            {"LEFT": "n words", "RIGHT": "5 words"},
            {"LEFT": anon_context["LEFT"], "RIGHT": "5 words"},
        ),
        (
            {"LEFT": "5 words", "RIGHT": "n words"},
            {"LEFT": "5 words", "RIGHT": anon_context["RIGHT"]},
        ),
    ]:
        assert cwb.contextualize(obj[0]) == obj[1]


def test_aline():
    for obj in [
        (("A: B", ("[^:]+", r":\s+", r"\S.*")), {"k": "a", "v": "B"}),
        (("A = B", ("[^=]+", r"\s+=\s+", r"\S.*")), {"k": "a", "v": "B"}),
        (("A B", (r"\S+", r"\s+", r"\S.*")), {"k": "a", "v": "B"}),
        (("_A: B:!", ("[^:]+", r":\s+", r"\S.*")), {"k": "_a", "v": "B:!"}),
        (("_A = B=", ("[^=]+", r"\s+=\s+", r"\S.*")), {"k": "_a", "v": "B="}),
        (("_!A B !", (r"\S+", r"\s+", r"\S.*")), {"k": "_!a", "v": "B !"}),
        (("A: 0", ("[^:]+", r":\s+", r"\S.*")), {"k": "a", "v": 0}),
        (("A = true", ("[^=]+", r"\s+=\s+", r"\S.*")), {"k": "a", "v": True}),
        (("A Null", (r"\S+", r"\s+", r"\S.*")), {"k": "a", "v": None}),
        (("A: +1", ("[^:]+", r":\s+", r"\S.*")), {"k": "a", "v": 1}),
        (("A = -2.00", ("[^=]+", r"\s+=\s+", r"\S.*")), {"k": "a", "v": -2.0}),
        (("A 1,000", (r"\S+", r"\s+", r"\S.*")), {"k": "a", "v": 1000}),
    ]:
        assert cwb.aline(*obj[0]) == obj[1]


def test_pick():
    for iput, oput in [
        ([{"info": {"id": "A"}}], [[1, "A", True, True]]),
        (
            [{"info": {"id": "A"}}, {"info": {"id": "B"}}],
            [[1, "A", True, False], [2, "B", False, True]],
        ),
        (
            [
                {"info": {"id": "A"}},
                {"info": {"id": "B"}},
                {"info": {"id": "C"}},
            ],
            [
                [1, "A", True, False],
                [2, "B", False, False],
                [3, "C", False, True],
            ],
        ),
    ]:
        for i, res1 in enumerate(cwb.pick(iput).values()):
            assert list(res1.values()) == oput[i]


def test_corpora_list():
    raw = cwb.cqp("show corpora").stdout
    for obj in [
        (("", False), []),
        (("", True), []),
        ((cwb.cqp("FAIL").stdout, False), []),
        ((cwb.cqp("FAIL").stdout, True), []),
        ((cwb.cqp("show FAIL").stdout, False), []),
        ((cwb.cqp("show FAIL").stdout, True), []),
        ((raw, False), []),
        ((raw, True), [{"id_upper": "_TEST1"}, {"id_upper": "_TEST2"}]),
    ]:
        assert cwb.corpora_list(*obj[0]) == obj[1]


def test_info_dict():
    for obj in [
        ("", []),
        (cwb.cqp("FAIL").stdout, []),
        (cwb.cqp("info FAIL").stdout, []),
        (cwb.cqp("info _TEST1").stdout, ["charset", "properties", "size"]),
        (cwb.cqp("info _TEST2").stdout, ["charset", "properties", "size"]),
    ]:
        assert sorted(cwb.info_dict(obj[0])) == sorted(obj[1])


def test_context_descriptor_dict():
    keys = """left_context right_context corpus_position target_anchors
    positional_attributes structural_attributes aligned_corpora""".split()
    multiline_keys = """structural_attributes positional_attributes
    aligned_corpora""".split()
    for obj in [
        ("", [], None),
        (cwb.cqp("FAIL").stdout, [], None),
        (cwb.cqp("FAIL; show cd").stdout, keys, None),
        (cwb.cqp("_TEST1; show cd").stdout, keys, None),
        (cwb.cqp("_TEST2; show cd").stdout, keys, None),
    ]:
        res = cwb.context_descriptor_dict(obj[0])
        assert sorted(res) == sorted(obj[1])
        for key, val in res.items():
            if val:
                assert isinstance(val, list if key in multiline_keys else str)
            else:
                assert key in multiline_keys


def test_registry_dict():
    for obj in [
        ("", {}),
        ("# Ignore\n#Ignore\n   ## Ignore", {}),
        ("A B #Ignore\nC\tD#Leave", {"a": "B", "c": "D#Leave"}),
        ("attribute A #\tIgnore\nATTRIBUTE\tB", {"attribute": ["A", "B"]}),
        ("structure A\nSTRUCTURE\tB# Leave", {"structure": ["A", "B# Leave"]}),
        (get_asset(1, parse=False), get_asset(3)),
        (get_asset(2, parse=False), get_asset(4)),
    ]:
        assert cwb.registry_dict(obj[0]) == obj[1]


def test_sgml_dict():
    for obj in [
        ("", get_asset(2)),
        (get_asset(4, parse=False), get_asset(3)),
        (get_asset(5, parse=False), get_asset(3)),
        (get_asset(6, parse=False), get_asset(3)),
    ]:
        assert cwb.sgml_dict(obj[0], get_asset(1), get_asset(2)) == obj[1]
