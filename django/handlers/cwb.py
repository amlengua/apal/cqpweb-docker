import re
import hashlib
import subprocess
from pathlib import Path
from itertools import chain
from bs4 import BeautifulSoup
from django.conf import settings
from django.core.cache import cache
from django.core.paginator import Paginator
from django.utils.translation import gettext as _
from django.contrib.auth.models import AnonymousUser
from api.utils import retype

# TODO: Rewrite it using https://github.com/ausgerechnet/cwb-ccc

DEBUG = settings.DEBUG
USERS = settings.DCWB["USERS"]
S_TAG = settings.DCWB["CQP"]["S_TAG"]
TEXT_ID = settings.DCWB["CQP"]["TEXT_ID"]
PAGINATOR = settings.DCWB["CQP"]["PAGINATOR"]


def cqp(query=""):
    cache_id = hashlib.sha256(query.encode("utf-8")).hexdigest()
    cached = cache.get(cache_id) if not DEBUG else None
    if cached:
        return cached
    args = ["cqp", "-p", "-f"]
    result = subprocess.run(
        args,
        capture_output=True,
        timeout=10,
        text=True,
        input=query,
        shell=True,
        check=False,
    )
    result.stdout = re.sub(r"^[^\n]+>", "", result.stdout).strip()
    cache.set(cache_id, result) if not DEBUG else None
    return result


def registry(corpus=""):
    path = Path("/usr/local/share/cwb/registry/") / corpus.lower()
    text = path.read_text() if path.exists() else ""
    return registry_dict(text)


def info(corpus=""):
    res = cqp("info %s" % corpus.upper())
    return info_dict(res.stdout)


def context_descriptor(corpus=""):
    res = cqp("%s; show cd" % corpus.upper())
    return context_descriptor_dict(res.stdout)


def all_info(corpus=""):
    # TODO: Add corpus total docs as all_info["extra"]["total_docs"]
    all_info, corpus = {}, corpus.upper()
    for fn_name in ["registry", "info", "context_descriptor"]:
        all_info[fn_name] = globals()[fn_name](corpus)
    return all_info


def about():
    return settings.DCWB


def corpora():
    corpora = corpora_list(cqp("show corpora").stdout, DEBUG)
    corpora = [(c | registry(c["id_upper"])) for c in corpora]
    if corpora:
        return {"list": corpora}
    else:
        return {"error": _("Ningún corpus disponible.")}


def search(querydict, user=None):
    user = AnonymousUser() if not user else user
    querydict = validate(querydict)
    # TODO: Measure execution time and save it in seconds in
    #   data["meta"]["result"]["exec_time"] and add them in div#stats
    data = {"meta": {"search": querydict.dict()}, "concordance": []}
    if not querydict["status"]:
        return data
    corpus = data["meta"]["search"]["corpus"]
    descriptor = context_descriptor(corpus)
    context = contextualize(grant(user, "context"))
    query = sanitize(querydict["query"], querydict["mode"])
    res = cqp(requery(query, corpus, descriptor, context))
    data = sgml_dict(res.stdout, all_info(corpus), data)
    data["meta"]["search"]["msg"] = _("Búsqueda exitosa.")
    if not data["concordance"] or res.returncode == 1:
        data["meta"]["search"]["status"] = False
        msg = _("Tu búsqueda no arrojó ningún resultado.")
        if querydict["mode"] != "simple" and res.stderr:
            msg = res.stderr
        data["meta"]["search"]["msg"] = msg
    return data


def validate(querydict):
    querydict.setdefault("corpus", "")
    querydict.setdefault("query", "")
    querydict.setdefault("mode", "simple")
    querydict.setdefault("output", "html")
    querydict.setdefault("page", 1)
    corpus_exists = True if info(querydict["corpus"]) else False
    if corpus_exists and querydict["query"]:
        querydict["query"] = str(querydict["query"])
        querydict["status"] = True
        querydict["msg"] = _("Término de búsqueda válido.")
        querydict["corpus"] = querydict["corpus"].upper()
        try:
            querydict["page"] = int(querydict["page"])
        except Exception:
            querydict["page"] = 1
    else:
        querydict["status"] = False
        if not corpus_exists and not querydict["query"]:
            msg = _("Se requiere un corpus y al menos un término de búsqueda.")
        elif not corpus_exists:
            msg = _("Se requiere indicar un corpus válido.")
        else:
            msg = _("Se requiere al menos un término de búsqueda.")
        querydict["msg"] = msg
    return querydict


def sanitize(raw_query, mode):
    # TODO: Add spaces before and after punctuation (\p{P} or [:punct:];
    #   e.g. "perro," to "perro ,"
    #   Try to avoid https://github.com/mrabarnett/mrab-regex
    query, ban = "", ["set", "show", '".*"', '".+"', "'.*'", "'.+'"]
    rgx = r'(?P<ini>[^"]*)(?P<lit>"[^"]*")*(?P<end>.*)'
    ok = True if re.sub(r"\W", "", raw_query) else False
    raw_query = raw_query.strip()
    raw_query = raw_query[:-1] if raw_query[-1] == ";" else raw_query
    if ok and mode == "simple":
        res = re.match(rgx, raw_query).groupdict()
        res = {k: v.strip() for k, v in res.items() if v and v.strip()}
        res = {k: v[1:-1] if k == "lit" else v for k, v in res.items()}
        res = {k: v.replace('"', '"') for k, v in res.items()}
        for kind, match in res.items():
            queries = [f'"{q}"' for q in match.split()]
            queries = [f"{q}%cd" if kind != "lit" else q for q in queries]
            query += " ".join(queries) + " "
        # TODO: Following replacement requires conditional so it scapes
        #   ., *, ?, ! and other RegExp when there are the only character
        query = query.strip().replace("*", ".*")
    elif ok and mode != "simple":
        for word in ban:
            raw_query = raw_query.replace(word, "")
        query = re.sub(r"\s+", " ", raw_query.strip())
    return query


def requery(query, corpus, context_descriptor, user_context):
    attrs = context_descriptor["positional_attributes"]
    attrs = [] if not attrs else attrs
    return (
        "; ".join(
            [
                corpus,
                'set pm "sgml"',
                f'set ps "{TEXT_ID}"',
                'set AttributeSeparator "\t"',
                f"set LeftContext {user_context['LEFT']}",
                f"set RightContext {user_context['RIGHT']}",
                *["show +" + attr["name"] for attr in attrs],
                f"{query} within {S_TAG}",
            ]
        )
        + ";"
    )


def grant(user, key):
    is_staff, is_authenticated = user.is_staff, user.is_authenticated
    user_type = "STAFF" if is_staff else "AUTH" if is_authenticated else "ANON"
    key, grants = key.upper(), USERS[user_type]
    if key not in grants:
        raise ValueError(f"Key {key} not found in {grants}")
    return grants[key]


def contextualize(context):
    default = grant(AnonymousUser(), "context")
    if "BOTH" in default:
        default = {"LEFT": default["BOTH"], "RIGHT": default["BOTH"]}
    if "BOTH" in context:
        context = {"LEFT": context["BOTH"], "RIGHT": context["BOTH"]}
    elif "LEFT" not in context or "RIGHT" not in context:
        context = default
    for key, val in context.items():
        if key not in ["LEFT", "RIGHT"]:
            del context[key]
        else:
            num, string = re.split(r"\s+", val)
            if not num.isdigit() or string not in ["sentences", "words"]:
                context[key] = default[key]
            if string == "sentences":
                context[key] = f"{num} s"
    return context


def aline(string, rgxs):
    regex = re.compile(f"(?P<key>{rgxs[0]}){rgxs[1]}(?P<val>{rgxs[2]})")
    res = {"k": "", "v": ""}
    key_val = re.match(regex, string)
    if key_val:
        res["k"] = "_".join(key_val.group("key").strip().lower().split())
        res["v"] = retype(key_val.group("val"))
    return res


def pick(concordance):
    docs, ids = {}, {item["info"]["id"] for item in concordance}
    for index, doc_id in enumerate(sorted(ids)):
        # TODO: Add more doc metadata
        docs.setdefault(
            doc_id,
            {
                "index_": index + 1,
                "id": doc_id,
                "is_first_": index == 0,
                "is_last_": index + 1 == len(ids),
            },
        )
    return docs


def corpora_list(raw, with_tests):
    # NOTE: Corpora names start at index 3
    raw_corpora = re.split(r"\s+", raw)[3:]
    if with_tests:
        return [{"id_upper": e} for e in raw_corpora]
    else:
        return [{"id_upper": e} for e in raw_corpora if e[0] != "_"]


def info_dict(raw):
    info, active_key = {}, None
    lines = raw.strip().split("\n\n")[0].split("\n")
    for line in lines:
        solo_key = re.match(r"(?P<key>[^:]+):", line)
        pair1 = aline(line, ("[^:]+", r":\s+", r"\S.*"))
        pair2 = aline(line, ("[^=]+", r"\s+=\s+", r"\S.*"))
        if pair1["k"]:
            active_key = None
            info.setdefault(pair1["k"], pair1["v"])
        elif solo_key:
            active_key = solo_key.group("key").strip().lower()
            info.setdefault(active_key, {})
        elif pair2["k"]:
            info[active_key].setdefault(pair2["k"], pair2["v"])
    return info


def context_descriptor_dict(raw):
    cd, active_key = {}, None
    lines = [ln.split("\n") for ln in raw.strip().split("\n\n")[1:-1]]
    multiline_keys = """structural_attributes positional_attributes
    aligned_corpora""".split()
    for line in list(chain(*lines)):
        pair = aline(line, ("[^:]+", r":\s+", r"\S.*"))
        val = pair["v"].split() if pair["v"] else line.split()
        has_val = val[-1] != "<none>"
        if pair["k"] and pair["k"] in multiline_keys:
            cd.setdefault(pair["k"], [] if has_val else None)
            active_key = pair["k"]
        elif pair["k"]:
            cd.setdefault(pair["k"], pair["v"])
            active_key = None
        if active_key and has_val:
            # TODO: Check output for aligned_corpora and change accordingly
            vals = (val[-1], val[0]) if val[-1] == "[A]" else (val[0], val[-1])
            val_dict = {"name": vals[1]}
            if active_key == multiline_keys[0]:
                val_dict["attribute"] = vals[0] == "[A]"
            else:
                val_dict["default"] = vals[0] == "*"
            cd[active_key].append(val_dict)
    return cd


def registry_dict(raw):
    registry = {}
    for line in [ln.strip() for ln in raw.strip().split("\n")]:
        pair = aline(line, (r"\S+", r"\s+", r"\S.*"))
        if line and line[0] != "#" and pair:
            pair["v"] = re.sub(r"\s+#.*", "", pair["v"])
            if pair["k"] in ["attribute", "structure"]:
                registry.setdefault(pair["k"], [])
                registry[pair["k"]].append(pair["v"])
            else:
                registry.setdefault(pair["k"], pair["v"])
    return registry


def sgml_dict(sgml, corpus_info, data):
    # TODO: Remove doc ID from concordance
    rgx_id, rgx_kv = r"<\S+\s+(?P<id>\S+)>", r"([^=\|]+)=([^=\|]+)"
    docs, soup = set(), BeautifulSoup(sgml, "html.parser").find_all("line")
    p_attrs_dict = corpus_info["context_descriptor"]["positional_attributes"]
    p_attrs = [attr["name"] for attr in p_attrs_dict]
    for line in soup:
        pos, el = "left", {"info": {}, "left": [], "center": [], "right": []}
        tokens = line.find_all("token")
        for index, tkn in enumerate(tokens):
            token, parent, parts = {}, tkn.parent.name, tkn.string.split("\t")
            # TODO: Ensure that tokens referending docs ID are ignored
            pos = "center" if pos == "left" and parent == "match" else pos
            pos = "right" if pos == "center" and parent != "match" else pos
            for i, attr in enumerate(parts):
                finds = re.findall(rgx_kv, attr)
                val = {f[0]: f[1] for f in finds} if finds else attr
                token[p_attrs[i]] = val
            token["index_"] = index + 1
            token["is_first_"] = index == 0
            token["is_last_"] = token["index_"] == len(tokens)
            el[pos].append(token)
        el["info"]["id"] = re.match(rgx_id, line.find("strucs").string)["id"]
        el["info"]["line"] = retype(line.find("matchnum").string)
        el["info"]["index_start"] = -len(el["left"])
        data["concordance"].append(el)
        docs.add(el["info"]["id"])
    abs_freq = len(data["concordance"])
    pmw_freq = round(abs_freq / corpus_info["info"]["size"] * 1000000, 2)
    pp = Paginator(data["concordance"], PAGINATOR)
    if data["meta"]["search"]["page"] in pp.page_range and data["concordance"]:
        page = pp.page(data["meta"]["search"]["page"])
        p_key = next(filter(lambda p: p["default"], p_attrs_dict))["name"]
        data["concordance"] = page.object_list
        data["meta"]["result"] = {}
        data["meta"]["result"]["default_p"] = p_key
        data["meta"]["result"]["items_index"] = page.start_index()
        data["meta"]["result"]["abs_freq"] = abs_freq
        data["meta"]["result"]["pmw_freq"] = pmw_freq
        data["meta"]["result"]["total_docs"] = len(docs)
        data["meta"]["result"]["total_pages"] = pp.num_pages
        data["meta"]["result"]["pagination"] = list(pp.get_elided_page_range())
        data["meta"]["result"]["docs"] = pick(data["concordance"])
    else:
        data["concordance"] = []
    return retype(data)
