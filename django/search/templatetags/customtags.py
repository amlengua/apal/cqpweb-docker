from django import template
from django.urls import reverse
from urllib.parse import urlencode

register = template.Library()


@register.simple_tag
def concat(*args, separator=""):
    """Concatenates values"""
    strs = [str(a) for a in args]
    return separator.join(strs)


# NOTE: This is used by django/templates/common/main.html
@register.simple_tag
def split(val, separator=""):
    """Splits values"""
    return val.split(separator)


@register.simple_tag
def to_query(key, val, root=None):
    if root:
        pair = f"{root}='.*{key}={val}.*'"
    else:
        pair = f"{key}='{val}'"
    return f"[{pair} %cd]"


@register.simple_tag
def to_url(url_name, **kwargs):
    """Encodes url_name kwargs as url with query parameters"""
    return reverse(url_name) + "?" + urlencode(kwargs)
