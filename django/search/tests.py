from tests.common import assert_templates, assert_context
from django.test import Client


def test_search_view():
    c = Client()
    response = c.post("/search/")
    assert response.status_code == 200  # noqa: PLR2004
    assert_templates(
        response.templates,
        """common/main common/user-button common/hero search/results
        search/search-bar""",
    )
    assert_context(
        response.context, {"corpora": dict, "page_name": str, "results": dict}
    )
