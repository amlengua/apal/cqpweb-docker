from handlers import cwb
from django.shortcuts import render
from django.utils.translation import gettext as _


def search(request, res=None):
    res = cwb.search(request.GET.copy(), request.user) if not res else res
    context = {
        "page_name": _("Búsqueda"),
        "corpora": cwb.corpora(),
        "results": res,
    }
    return render(request, "search/results.html", context)
