.SILENT:

update:
	git submodule init
	git submodule update
	sassc -t compressed assets/main.scss assets/static/css/bulma.min.css
	python3 django/manage.py collectstatic --noinput
	python3 django/manage.py makemigrations --noinput
	python3 django/manage.py migrate --noinput
	python3 django/manage.py createcachetable
	python3 django/manage.py clearcache
	python3 django/manage.py makemessages -l en_US # NOTE: Related to settings.LANGUAGES
	python3 django/manage.py compilemessages -v 0

run: update
	python3 django/manage.py runserver

format:
	pandoc --markdown-headings=atx --reference-links --reference-location=document --columns=79 -o README.md README.md
	python3 -m djlint django --reformat --quiet --warn --configuration djlint.toml
	python3 -m ruff format django django/dcwb/settings.py --config ruff.toml

validate: format
	python3 django/manage.py validate_templates
	python3 django/manage.py check
	python3 -m djlint django --lint --check --warn --configuration djlint.toml
	python3 -m ruff check django django/dcwb/settings.py --exit-zero --fix --config ruff.toml

test: update validate
	python3 -m pytest -s django/tests
