# DjangoCWB

\[TOC\]

## Español

DjangoCWB (`dcwb`) es una plataforma web hecha con [Django] para el [IMS Open
Corpus Workbench] (CWB). Este proyecto es financiado por la [Academia Mexicana
de la Lengua].

### Requisitos

-   [CWB][IMS Open Corpus Workbench] == 3.5.1
-   Python \>= 3.12
-   [Django] == 4.2
-   pip
-   pandoc
-   sassc

### Uso

> Una manera más sencilla de usar es mediante la imagen de Docker de DjangoCWB.
> Ve [aquí] para más información.

Una vez instalados los requisitos:

1.  Comprueba que tienes instalado cwb: `cwb-config --version`.
2.  Comprueba que los datos y los registros de CWB están en
    `/usr/local/share/cwb`; para corpora de pruebas puedes copiar `assets/cwb`
    en `/usr/local/share`.
3.  Copia `django/dcwb/settings.py.bak` en `django/dcwb/settings.py`.
4.  Modifica `django/dcwb/settings.py` según tus necesidades.
5.  Instala y activa un [entorno virtual].
6.  Instala dependencias: `pip install -r requirements.txt`.
7.  Actualiza DjangoCWB: `make update`.
8.  Crea un superusuario: `python django/manage.py createsuperuser`.
9.  Comienza servidor de Django con `make run` o haz pruebas con `make test`.

### Licencia

Este proyecto es *software* libre bajo licencia [GPLv3].

  [Django]: https://www.djangoproject.com
  [IMS Open Corpus Workbench]: https://cwb.sourceforge.io
  [Academia Mexicana de la Lengua]: https://academia.org.mx
  [aquí]: https://gitlab.com/amlengua/apal/dcwb-docker
  [entorno virtual]: https://docs.python.org/3/library/venv.html
  [GPLv3]: https://gitlab.com/amlengua/apal/dcwb
